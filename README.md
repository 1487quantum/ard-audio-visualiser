# ard-audio-visualiser
An Arduino project that uses a RGB matrix shield &amp; Processing to show the visualisation of an audio file. The audio file is loaded in Processing (v2.2.1), which will then output the data to the RGB matrix via Serial Tx (Transmission).
<p/>
<img src="https://github.com/1487quantum/ard-audio-visualiser/blob/master/demo.gif" width="75%"></img>
<p>The code may be <B>unstable</b> at times, please use with <b>caution</b>!</p>
<img src="https://github.com/1487quantum/ard-audio-visualiser/blob/master/demo2.gif"></img>

##Libraries
####Arduino
- Rainbowduino

####Processing
- controlP5
- Minim
