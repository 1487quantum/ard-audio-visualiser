import ddf.minim.*;
import ddf.minim.analysis.*;
import java.io.File;
import java.io.FilenameFilter;
import controlP5.*;
import processing.serial.*; //library for serial communication

Serial port; //creates object "port" of serial class

ControlP5 cp5;

Minim minim;
AudioPlayer song;
AudioMetaData meta;
FFT fft;

String filename;

int btnWidth = 120;
int btnHeight = 40;
int padding = 10;
float scaleFactor = 0.75; 

int[] freq_array = {
  0, 0, 0, 0, 0, 0, 0, 0
};

int count = 0;

boolean firstStart = true;
boolean isSelected = false;

void setup()
{
  size(350, 350);
  // always start Minim first!
  minim = new Minim(this);

  //Title
  frame.setTitle("Arduino Audio Visualiser");

  port = new Serial(this, Serial.list()[0], 9600); //set baud rate

  cp5 = new ControlP5(this);

  // create a new button with name 'Select_Music'
  cp5.addButton("Select_Music")
    .setValue(0)
      .setPosition(getWidth()-(btnWidth+padding), padding)
        .setSize(btnWidth, btnHeight)
          ;
}


// function Select_Music will receive changes from 
// controller with name Select_Music
public void Select_Music(int theValue) {

  if (!firstStart) {
    if (isSelected) {
      cp5.controller("Select_Music").setLock(true);
      cp5.controller("Select_Music").setCaptionLabel("Music Selected");
      return;
    }
    selectInput("Import music file", "fileSelected");
  } else {
    firstStart = false;
  }
}

/* Taken from Processing.org */
void fileSelected(File selection) {
  if (selection == null) {
    println("Window was closed or user hit cancel");
  } else {
    filename = selection.getAbsolutePath();
    loadSong(filename, "");
    isSelected = true;
  }
}

void loadSong(String directory, String songName) {

  // specify 512 for the length of the sample buffers
  // the default buffer size is 1024
  song = minim.loadFile(directory, 1024);
  meta = song.getMetaData();
  song.play();

  // an FFT needs to know how 
  // long the audio buffers it will be analyzing are
  // and also needs to know 
  // the sample rate of the audio it is analyzing
  fft = new FFT(song.bufferSize(), song.sampleRate());

  /*-----------Future Implementation-----------------*/
  // calculate the averages by grouping frequency bands linearly. use 30 averages.
  //fft.linAverages( 30 );
  
}

void draw()
{
  background(0);
  if (isSelected) {
    if(song.isPlaying()){
    text("Playing", padding, 2*padding);
    }else{
          text("Stopped", padding, 2*padding);

    }
    try {
      File f = new File(meta.fileName());
      if (song.isPlaying()) {
        text(f.getName().toString(), padding, getHeight()-padding);
      } else {
        text("", padding, getHeight()-padding);
      }
    }
    catch (ArrayIndexOutOfBoundsException e) {
      println("Array is out of Bounds"+e);
    }

    //DRAW FFT
    fft.forward(song.mix);
    colorMode(HSB, 255);

    float spread = map(350, 0, width, 1, 21.5);
    float x = 0;

    for (int k=1; k<9; k++) {
      //Level
      stroke(0, 0, 255);
      line(2*padding, scaleFactor*getHeight() - 20*k, getWidth()-2*padding, scaleFactor*getHeight() - k* 20);
    }

    for (int i = 0; i < song.sampleRate ()&& x < width; i += spread)
    {
      x = i/spread;
      if (i%8==1) {   
 
        //println(fft.getFreq(i));
        if (count<8) {
          freq_array[count] = constrain((int) map(int(fft.getFreq(i)), 0, 80, 0, 8), 0, 8);
          //println(count);
          stroke(255, 0, 255); //White
          textSize(15);
          text(freq_array[count], x*5+10, scaleFactor*getHeight()+20);
          for (int q=0; q<30; q++) {
            line(x*5+q, scaleFactor*getHeight(), x*5+q, scaleFactor*getHeight() - freq_array[count] * 20);
          }
        }
        count++;
      }
    }
    count=0;  //Reset count

    if (!song.isPlaying()) {
      port.write(0);
    } else {
      //send to serial
      for (int i=0; i<8; i++) {
        port.write(freq_array[i]);
      }
    }
  }
}

